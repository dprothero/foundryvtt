.. _canvas:


The Game Canvas
***************

The virtual tabletop environment in Foundry VTT is implemented using an HTML5 canvas. 
The canvas itself is constructed using an ordered sequence of layers. 
Understanding the canvas layers will be helpful when building within the application.

..  toctree::
    :caption: Canvas Layers
    :name: canvas-layers
    :maxdepth: 1

    canvas/background
    canvas/grid
    canvas/walls
    canvas/tokens
