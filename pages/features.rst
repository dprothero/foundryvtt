..	_features:

Anticipated Features and Timelines
**********************************

The following table attempts to articulate the major features which I am working to implement for various phases of development. I have classified features under one of three major milestones: **Alpha**, **Beta**, and **Launch**. I am currently working through the backlog of stories which I have scoped for the Alpha milestone.

To keep track of the the current features which are being worked on or planned, please see the public story board on GitLab https://gitlab.com/foundrynet/foundryvtt/boards.

..  csv-table::
    :file: ../_static/features.csv
    :widths: 20, 60, 15, 15
    :header-rows: 1
