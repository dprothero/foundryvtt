..  _ui:

Creating UI Elements
********************

A key aspect of developing systems, modules, or custom interfaces for your world is defining and rendering UI
elements which give your game-master or players access to and control over underlying game data.

The key building block of UI elements in Foundry VTT is the :class:`Application` class which provides a core
framework for rendering blocks of HTML into the tabletop client. The pages within this section provide 
documentation on how to create Applications using this framework.

..  toctree::
    :caption: UI Concepts
    :maxdepth: 1

    apps/application.rst
    apps/forms.rst
    apps/templating.rst
    apps/dialog.rst
    apps/notifications.rst
    apps/helpers.rst
