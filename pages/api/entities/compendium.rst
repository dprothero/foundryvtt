.. _compendiumAPI:


Compendium Content Packs
************************

Module developers can interact with compendum packs in a programmatic way using the API offered by the
:class:`Compendium` class.

-----

The Compendium Pack Class
=========================

.. autoclass:: Compendium
	:members:

